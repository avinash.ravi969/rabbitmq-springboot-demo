/**
 * 
 */
package com.example.rabitmq.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * @author Avinash
 *
 */
@RestController
public class MessageController {
	
	private static final Logger log = LoggerFactory.getLogger(MessageController.class);

	private final RabbitTemplate rabbitTemplate;
	private ProducerConfig producerConfig;

	public ProducerConfig getProducerConfig() {
		return producerConfig;
	}

	@Autowired
	public void setProducerConfig(ProducerConfig producerConfig) {
		this.producerConfig = producerConfig;
	}

	@Autowired
	public MessageController(final RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public String handleMessage(@RequestParam("name") String name) {
    	
    	String exchange = getProducerConfig().getApp1Exchange();
		String routingKey = getProducerConfig().getApp1RoutingKey();

		/* Sending to Message Queue */
		try {
			rabbitTemplate.convertAndSend(exchange, routingKey, name);
			return "Name Sent Successfully";
			
		} catch (Exception ex) {
			log.error("Exception occurred while sending message to the queue. Exception= {}", ex);
			return "Failure";
		}
    	
    	//rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
//        rabbitTemplate.convertAndSend("spring-boot-exchange", "q2.logs", name);
//        return "Message sent to RabbitMQ: Hello from "+name;

    }

//    @MessageMapping("/greetings")
//    @SendTo("/topic/greetings")
//    public ContentMessage greeting(HelloMessage message) throws Exception {
//        return produceMessage(message.getName());
//    }

   // @RabbitListener(queues = "${rabbitmq.queueName}")
//    @RabbitListener(queues = "#{'${rabbitmq.queueName}'.split(',')}" )
//   // @RabbitListener(queues = {"q2", " )
//    public void receiveMessage(String message) throws InterruptedException {
//    	Thread.sleep(20000);
//        messagingTemplate.convertAndSend("/topic/greetings", produceMessage(message));
//    }
    
 //   @RabbitListener(queues = "q3" )
//    public void receiveMessage(String message) throws InterruptedException {
//    	Thread.sleep(20000);
//        messagingTemplate.convertAndSend("/topic/greetings", produceMessage(message));
//    }

    private ContentMessage produceMessage(String message){
        return new ContentMessage("Hello, "+message);
    }
}
