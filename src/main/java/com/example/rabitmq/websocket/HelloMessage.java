package com.example.rabitmq.websocket;

/**
 * 
 * @author Avinash
 *
 */
public class HelloMessage {
    private String name;

    public HelloMessage() {

    }

    public HelloMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HelloMessage [name=" + name + "]";
	}
}
