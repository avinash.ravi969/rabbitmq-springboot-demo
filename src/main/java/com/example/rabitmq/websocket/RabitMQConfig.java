/**
 * 
 */
package com.example.rabitmq.websocket;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
//import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @author Avinash
 *
 */
////@EnableRabbit
//@Configuration
//public class RabitMQConfig  {
//	
////    @Value("${rabbitmq.queueName}")
////    String queueName;
////    
////    @RabbitListener(queues = "q1" )
////    
////    @RabbitListener(queues = "q2" )
////
////    @Bean
////    Queue queue1() {
////        return new Queue("q1", true, false, false);
////    }
////
////    @Bean
////    Queue queue2() {
////        return new Queue("q2", true, false, false); 
////    }
////    
////    @Bean
////    TopicExchange exchange() {
////        return new TopicExchange("spring-boot-exchange");
////    }
////
////    @Bean
////    Binding binding1(TopicExchange exchange) {
////        return BindingBuilder.bind(queue1()).to(exchange).with("q1");
////    }
////
////    @Bean
////    Binding binding2(TopicExchange exchange) {
////        return BindingBuilder.bind(queue2()).to(exchange).with("q2.logs");
////    }
//    
////    @Bean
////    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
////    final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
////    rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
////    return rabbitTemplate;
////    }
////    @Bean
////    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
////    return new Jackson2JsonMessageConverter();
////    }
////    @Bean
////    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
////    return new MappingJackson2MessageConverter();
////    }
////    @Bean
////    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
////    DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
////    factory.setMessageConverter(consumerJackson2MessageConverter());
////    return factory;
////    }
////    @Override
////    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
////    registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
////    }
////
//////    
//////  @Bean
//////  public ConnectionFactory connectionFactory() {
//////	  CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
//////	    connectionFactory.setHost("localhost");
//////	    connectionFactory.setUsername("guest");
//////	    connectionFactory.setPassword("guest");
//////
//////	    return connectionFactory;
//////  }
//////    
//////  @Bean
//////	public RabbitTemplate fixedReplyQRabbitTemplate() {
//////		RabbitTemplate template = new RabbitTemplate(connectionFactory());
//////		template.setExchange("spring-boot-exchange");
//////		template.setRoutingKey("q2.logs");
////////		template.setM
////////		//template.setUserCorrelationId(true);
////////		template.setReplyAddress("spring-boot-exchange" + "/" + "q2.logs");
//////		return template;
//////	}
////
////  /**
////   * @return The listener container that handles the request and returns the reply.
////   */
//////  @Bean
//////  public SimpleMessageListenerContainer serviceListenerContainer() {
//////      SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//////      container.setConnectionFactory(connectionFactory());
//////      container.setQueues(queue2());
//////      container.setMessageListener(new MessageListenerAdapter(new MessageController()));
//////      return container;
//////  }
////  
//////  @Bean
//////	public SimpleMessageListenerContainer replyListenerContainer() {
//////		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//////		container.setConnectionFactory(connectionFactory());
//////		container.setQueues( queue2());
//////		//container.setMessageListener(fixedReplyQRabbitTemplate());
//////		return container;
//////	}
////  
//////    @Bean
//////    public RabbitTemplate amqpTemplate() {
//////        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
//////        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
//////        rabbitTemplate.setReplyTimeout(60000);
//////        return rabbitTemplate;
//////    }
////
//////    @Bean
//////    public SimpleMessageListenerContainer replyListenerContainer() {
//////        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//////        container.setConnectionFactory(connectionFactory());
//////        container.setQueues(queue1(), queue2());
//////       // container.setMessageListener(amqpTemplate());
//////        return container;
//////    }
////    
////    
//////    @Override
//////    public void configureRabbitListeners(
//////            RabbitListenerEndpointRegistrar registrar) {
//////        registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
//////    }
//////    
//////    @Bean
//////    public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
//////        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
//////        factory.setMessageConverter(new MappingJackson2MessageConverter());
//////        return factory;
//////    }
////    
//////    @Bean
//////    public MessageController eventResultHandler() {
//////        return new MessageController();
//////    }
////  
//////    @Bean
//////    public MessageConverter jsonConverter() throws AmqpException {
//////        Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter();
//////        DefaultClassMapper mapper = new DefaultClassMapper();
//////        mapper.setDefaultType(HelloMessage.class);
//////        mapper.setDefaultType(ContentMessage.class);
//////        converter.setClassMapper(mapper);
//////        return converter;
//////    }
////    
//////    @Bean
//////    public ConnectionFactory connectionFactory() {
//////        return new CachingConnectionFactory("localhost");
//////    }
//////
//////    
//////    @Bean
//////    public RabbitTemplate rabbitTemplate() {
//////        RabbitTemplate template = new RabbitTemplate(connectionFactory());
//////        //template.setRoutingKey("q*");
//////        template.setMessageConverter(new Jackson2JsonMessageConverter());
//////        return template;
//////    }
////    
//////    @Bean
//////    public List<Queue> qs() {
//////    	return Arrays.asList(
//////    			new Queue("q2", false, false, false),
//////    			new Queue("q3", false, false, false)
//////    	);
//////    }
//////
//////    @Bean
//////    TopicExchange exchange() {
//////        return new TopicExchange("spring-boot-exchange");
//////    }
//////
//////    @Bean
//////    Binding binding(Queue queue, TopicExchange exchange) {
//////        return BindingBuilder.bind(queue).to(exchange).with("q*");
//////    }
////
//////    
//////    public MessageConverter getJsonMessageConverter() {
//////        return new Jackson2JsonMessageConverter();
//////      }
//////    @Bean
//////    MqReceiver mqReceiver() {
//////        return new MqReceiver();
//////    }
////
//////    @Bean
//////    MessageListenerAdapter listenerAdapter(MqReceiver mqReceiver) {
//////        return new MessageListenerAdapter(mqReceiver, "receiveMessage");
//////    }
//
//}
