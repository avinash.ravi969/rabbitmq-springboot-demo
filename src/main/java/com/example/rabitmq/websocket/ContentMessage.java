/**
 * 
 */
package com.example.rabitmq.websocket;

/**
 * @author Avinash
 *
 */
public class ContentMessage {
    private String content;


    public ContentMessage() {
	}

	public ContentMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContentMessage [content=" + content + "]";
	}
    
    
}
